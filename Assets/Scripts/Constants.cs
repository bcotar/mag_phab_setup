﻿using System;

public static class Constants
{
	public static String pathJson = "/storage/emulated/0/Android/data/com.boco.pwa.setup/files/Data.json";
	public static String newTexture = "/storage/emulated/0/Android/data/com.boco.pwa.play/files/newTexture.png";
	public static String pathTextureThumb = "/storage/emulated/0/Android/data/com.boco.pwa.play/files/textureThumb.png";
	public static String pathTextureSelected = "/storage/emulated/0/Android/data/com.boco.pwa.play/files/textureSelected.png";
	public static String screenshotsDir = "/storage/emulated/0/Pictures/Tango";
	public static String xmlPath = "/storage/emulated/0/Android/data/com.boco.pwa.setup/files";

	public static bool scannerRunning = true;
	public static int galeryStatueID;
	public static int StatueIndex;
}

